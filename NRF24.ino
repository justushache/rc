#include <SPI.h>
#include <RF24_STM32.h>
#include <nRF24L01_STM32.h>

RF24 radio (PB0,PB1);// CE,CSN,IRQ
const uint64_t rxaddr = 133713371337;



int calculateChecksum(){
  return data.lx+
  data.ly+
  data.rx+
  data.ry;
  }

void setupNRF(){
  SPI.begin();
  SPI.setDataMode(SPI_MODE0);
  SPI.setBitOrder(MSBFIRST);

  radio.begin();
  radio.openWritingPipe(rxaddr);
  radio.stopListening();
}
  
void sendNRF(){
  radio.write(&data,sizeof(data));
  Serial.println(data.rx);
  Serial.println(data.ry);
  Serial.println(data.lx);
  Serial.println(data.ly);
  Serial.println();
  Serial.println();
}
