int calculateChecksum();
void setupNRF();

typedef struct {//MUST NOT BE BIGGER THAN 32 BYTES
  uint16_t lx;   //x-value of the left joystick in one-tenth of a percent
  uint16_t ly;   //y-value of the left joystick in one-tenth of a percent
  uint16_t rx;   //x-value of the right joystick in one-tenth of a percent
  uint16_t ry;   //y-value of the right joystick in one-tenth of a percent  
  bool sw1;
  bool sw2;
  bool sw3;
  bool sw4;
  bool sw5;
  bool sw6;
  bool sw7;
  bool sw8;
  bool sw9;
  bool sw10;  
  uint32_t checkSum;
}data_s;


data_s data;

void setup() {
  Serial.begin(115200);
  // put your setup code here, to run once:
  pinMode(PC13,OUTPUT);
  
  pinMode(PB12,INPUT_PULLUP);
  pinMode(PB13,INPUT_PULLUP);
  pinMode(PB14,INPUT_PULLUP);
  pinMode(PB15,INPUT_PULLUP);
  pinMode(PA8,INPUT_PULLUP);
  pinMode(PB9,INPUT_PULLUP);
  pinMode(PB8,INPUT_PULLUP);
  pinMode(PB5,INPUT_PULLUP);
  pinMode(PB4,INPUT_PULLUP);
  pinMode(PB3,INPUT_PULLUP);
  //gyroSetup();
  setupNRF();  
  }

void loop() {
  data.rx=analogRead(PA0)/4.096;
  data.ry=(analogRead(PA1)/4.096-29)*1.031;
  data.lx=analogRead(PA2)/4.096-5;
  data.ly=analogRead(PA3)/4.096;
  data.sw1=digitalRead(PB12);
  data.sw2=digitalRead(PB13);
  data.sw3=digitalRead(PB14);
  data.sw4=digitalRead(PB15);
  data.sw5=digitalRead(PA8);
  data.sw6=digitalRead(PB9);
  data.sw7=digitalRead(PB8);
  data.sw8=digitalRead(PB5);
  data.sw9=digitalRead(PB4);
  data.sw10=digitalRead(PB3);
  sendNRF();
  delay(20);
}
